from   benchmark import benchmark as b
import numpy as np
from   tkinter import *
from   tkinter import ttk
import matplotlib.pyplot as plt

class Checkbar(Frame):
	def __init__(self, parent=None, picks=[], side=LEFT, anchor=W):
			Frame.__init__(self, parent)
			self.vars = []
			for pick in picks:
				var = IntVar(value=1)
				chk = Checkbutton(self, text=pick, variable=var)
				chk.pack(side=side, anchor=anchor, expand=YES)
				self.vars.append(var)

	def state(self):
		return map((lambda var: var.get()), self.vars)

class application:

	def __init__(self):
		self.lstfiletypes = np.array(['TXT', 'Docx', 'MP3 V0', 'MP3 320','Flac', 'jpeg', 'bz', 'rnd'])
		self.lstmetodos = np.array(['7z', 'gzip', 'bzip2', 'zip'])

		self.main = Tk()
		self.main.geometry('480x480')
		self.main.title("Benchmark")

		self.lblmetodos = ttk.Label(self.main, text="Formatos de Compresión")
		self.lblmetodos.pack(side=TOP, fill=BOTH, expand=True, padx=5, pady=5)

		self.chkmetodos = Checkbar(self.main, self.lstmetodos)
		self.chkmetodos.pack(side=TOP,  fill=X)

		self.lblfiletypes = ttk.Label(self.main, text="Formatos de Archivos")
		self.lblfiletypes.pack(side=TOP, fill=BOTH, expand=True,padx=5, pady=5)

		self.filetypes = Checkbar(self.main, self.lstfiletypes)
		self.filetypes.pack(side=TOP,  fill=X)

		self.btnext = ttk.Button(self.main, text="Iniciar", command=self.aceptar)
		self.btnext.pack(side = RIGHT, fill = BOTH, expand=True, padx = 5, pady = 5)

		self.btnquit = ttk.Button(self.main, text="Salir", command=self.main.destroy)
		self.btnquit.pack(side = RIGHT, fill = BOTH, expand=True, padx = 5, pady = 5)

		self.imp_btn = []

		self.main.mainloop()

	def aceptar(self):
		cmetodos = np.array(list(self.chkmetodos.state()))
		posmetodos = np.where(cmetodos==1)

		cfiletypes = np.array(list(self.filetypes.state()))
		posfiletypes = np.where(cfiletypes==1)

		self.metodos = self.lstmetodos[posmetodos]
		self.formatos = self.lstfiletypes[posfiletypes]

		self.benchmarks = {}
		for formato in self.formatos:
			self.benchmarks[formato] = b.Benchmark(self.metodos, formato, 1)

		for benchmark in self.benchmarks.values():
			benchmark.execute()

		self.results()

	def results(self):
		self.vResults = Tk()
		self.vResults.geometry('500x300')
		self.vResults.title('Resultados')

		#self.lblformatos=ttk.Label(self.vResults, text="formatos", background="red", foreground = "white", anchor=CENTER, font=(None, 12))
		#self.lblformatos.grid(row=0, column=0, padx=1, pady=1, ipadx=50, ipady=0.5)
		#self.lblmetodos=ttk.Label(self.vResults, text="Metodos de compresion", background="red", foreground = "white", anchor=CENTER,font=(None, 12))
		#self.lblmetodos.grid(row=0, column=1, padx=1, pady=1, ipadx=80, ipady=0.5)
		for k, t in enumerate(self.benchmarks.keys()):
			btttn = ttk.Button(self.vResults,text="Ver", command = lambda : self.show_plots(t))
			btttn.grid(row=1+k, column=5, columnspan=2, sticky = 'ew')

		for x, column in enumerate(self.metodos):
			self.lbl = ttk.Label(self.vResults,text=column, font=(None, 14), background = "blue", foreground = "white",borderwidth=2, anchor=CENTER)
			self.lbl.grid(row=0,column=x+1, padx=1, pady=1, ipadx=20, ipady=0.5)

		for y, formato in enumerate(self.formatos):
			benchmark = self.benchmarks[formato]
			self.lbl = ttk.Label(self.vResults,text=formato, font=(None, 12), background = "blue", foreground = "white",borderwidth=2, anchor=CENTER)
			self.lbl.grid(row=y+1,column=0, padx=1, pady=1, ipadx=10, ipady=0.5)
			for x, score in enumerate(benchmark.scores.values()):
				self.lbl = ttk.Label(self.vResults, text = round(score, 3), font=(None, 12), background = "blue", foreground = "white",borderwidth=2, anchor=CENTER)
				self.lbl.grid(row = y + 1, column= x + 1, padx=1, pady=1, ipadx=10, ipady=0.5)

		self.vResults.mainloop

	def show_plots(self, filetype):
		t = self.benchmarks[filetype]
		x = t.fileSizes
		plt.ion()
		folder = filetype
		fig = plt.figure("Resultados {}".format(folder))

		fig.add_subplot(2, 1, 1, title  = 'Tasa')
		for method in self.metodos:
			y = t.rate[method]
			plt.plot(x, y)

			plt.legend(self.metodos)
			plt.show()

		fig.add_subplot(2, 2, 3, title = 'Compresión')
		for method in self.metodos:
			y = t.timesCompression[method]
			plt.plot(x, y)

			plt.legend(self.metodos)
			plt.show()

		fig.add_subplot(2, 2, 4, title = 'Decompresión')
		for method in self.metodos:
			y = t.timesDecompression[method]
			plt.plot(x, y)

			plt.legend(self.metodos)
			plt.show()
