from benchmark import benchmark as b
from generator import generator as g
import numpy as np
import matplotlib.pyplot as plt

#g.generate_docx(10)
#g.generate_txt(10)
#g.generate_random(10)

methods = ['gzip','7z', 'bzip2', 'zip']
folder = 'jpeg'
t = b.Benchmark(methods, folder, 5)
t.execute()

x = t.fileSizes
#plt.ion()

fig = plt.figure("Resultados {}".format(folder))

fig.add_subplot(1, 1, 1, title = 'Tasa')

for method in methods:
	y = t.rate[method]
	plt.plot(x, y)

plt.xlabel('Tamaño (kb)')
plt.ylabel('Tasa (%)')
plt.title('Tasa')
plt.legend(methods)
plt.show()

fig = plt.figure("Resultados {}".format(folder))

for method in methods:
	y = t.timesCompression[method]
	plt.plot(x, y)

plt.xlabel('Tamaño (kb)')
plt.ylabel('Tiempo (s)')
plt.title('Compresión')
plt.legend(methods)
plt.show()

fig = plt.figure("Resultados {}".format(folder))

for method in methods:
	y = t.timesDecompression[method]
	plt.plot(x, y, )
	
plt.xlabel('Tamaño (kb)')
plt.ylabel('Tiempo (s)')
plt.title('Decompresión')
plt.legend(methods)
plt.show()

input("press key to exit")