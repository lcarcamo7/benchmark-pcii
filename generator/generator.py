from docx import Document
import lorem
import os

# GENERATING TXT FILES
# generate txt files
def generate_txt(files):
	current_text = ""
	for i in range(0, files):
		current_text = create_txt(i + 1, current_text)

def generate_docx(files):
	current_text = ""
	for i in range(0, files):
		current_text = create_docx(i + 1, current_text)

def generate_random(files):
	for i in range(0, files):
		create_random(i)


def create_txt(size, current_text):
	fsize = 2 ** (size-1)
	i = 0
	while i < fsize:
		current_text += lorem.paragraph() + '\n'
		i += 1

	f = open('files/txt/original/file' + str(size) + '.txt', 'w+')
	f.write(current_text)
	f.close()
	return current_text

def create_docx(size, current_text):
	fsize = 2 ** (size-1)
	i = 0
	while i < fsize:
		current_text += lorem.paragraph() + '\n'
		i += 1

	document = Document()
	document.add_heading('Title', 0)
	document.add_paragraph(current_text)
	document.save('files/docx/original/file' + str(size) + '.docx')
	return current_text

def create_random(size):
	sz = 2**size
	with open('files/rnd/original/file' + str(size + 1) + '.rnd', 'wb') as fout:
		fout.write(sz) # replace 1024 with size_kb if not unreasonably large