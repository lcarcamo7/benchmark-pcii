import asyncio
import json
import numpy as np
import os
from   os.path import join, splitext
import shutil as sh
import subprocess
import time


formats = {'gzip': 'gz', '7z': '7z', 'bzip2' : 'bz2', 'zip': 'zip'}

# Return stats for the benchmark
# targetted at A SINGLE file format
class Benchmark:

	directory = 'files/'

	# fileFormat may be "folder" for compressing folders
	def __init__(self, methods, folder, tries):
		self.methods = methods
		self.tries =  tries

		# basic directories
		self.baseDir = Benchmark.directory + folder + "/"
		self.dirBaseCompression = self.baseDir + 'compressed/'
		self.dirBaseDecompression = self.baseDir + 'decompressed/'
		self.dirBaseOriginal = self.baseDir + 'original/'
		# Dictionaries to save  the rate and timing for each method
		self.rate = {}
		self.timesCompression = {}
		self.timesDecompression = {}
		# Averages
		self.avgrate = {}
		self.avgcomp = {}
		self.avgdeco = {}
		# Final scores
		self.scores = {}
		# Saves the file sizes
		self.fileSizes =  []
		self.__verifyFolders__()
		# files to be processed
		self.files =  np.array(os.listdir(self.dirBaseOriginal))
		self.totalFiles = len(self.files)
		self.__init_values__()

	# Initializes the dictionaries where the results for
	# rate and compression and decompression time will be stored
	def __init_values__(self):
		for method in self.methods:
			self.rate[method] = [0]*self.totalFiles
			self.timesCompression[method] = [0]*self.totalFiles
			self.timesDecompression[method] = [0]*self.totalFiles

	# creates required folders in  case they don't exists
	def __verifyFolders__(self):
		if not os.path.isdir(Benchmark.directory):
			os.mkdir(Benchmark.directory)

		if not os.path.isdir(self.baseDir):
			os.mkdir(self.baseDir)

		if not os.path.isdir(self.dirBaseCompression):
			os.mkdir(self.dirBaseCompression)

		if not os.path.isdir(self.dirBaseDecompression):
			os.mkdir(self.dirBaseDecompression)

	def execute(self):
		self.__calc_sizes__()
		self.__compress__()
		self.__rate__()
		self.__decompress__()
		self.__avg__()
		self.__saveresults__()

	def __avg__(self):
		for method in self.methods:
			self.avgrate[method] = np.mean(np.array(self.rate[method]))
			self.avgcomp[method] = np.mean(np.array(self.timesCompression[method]))
			self.avgdeco[method] = np.mean(np.array(self.timesDecompression[method]))
		
		for method in self.methods:
			self.scores[method] = 0.6*self.avgrate[method] + 1000*0.20*(self.avgdeco[method] + self.avgcomp[method])

	def __saveresults__(self):
		f = open(self.baseDir + 'results.txt', 'w+')

		f.write('File sizes\n')
		f.write('[')
		f.write(','.join(str(round(e, 3)) for e in self.fileSizes))
		f.write(']')

		f.write('Rate\n')
		f.write(json.dumps(self.rate))
		f.write('\n')

		f.write('Compression\n')
		f.write(json.dumps(self.timesCompression))
		f.write('\n')

		f.write('Decompression\n')
		f.write(json.dumps(self.timesDecompression))

		f.close()

	def __rate__(self):
		for method in self.methods:
			self.__method_rate__(method)

	def __compress__(self):
		for i in range(0, self.tries):
			self.__reset__(compression = True)
			times = self.__compression__()
			for method in self.methods:
				method_time = times[method]
				for k in range(0, self.totalFiles):
					self.timesCompression[method][k] += method_time[k]/self.tries

	def __decompress__(self):
		for i in range(0, self.tries):
			self.__reset__(decompression = True)
			times = self.__decompression__()
			for method in self.methods:
				method_time = times[method]
				for k in range(0, self.totalFiles):
					self.timesDecompression[method][k] += method_time[k]/self.tries

	def __compression__(self):
		# Inits the dictionary  where the lists of times for each method will
		# be save
		timesCompression = {}
		for method in self.methods:
			# Inits  as empty the dictionary entry for the [list of] timings for
			# the compression formath. It's  a  vector as it saves the time
			# for each file
			timesCompression[method] = []
			# file where the compressed files in the given current format
			# will be saved
			dirCompressed = self.dirBaseCompression + method + "/"
			for filename in self.files:
				# full path to the file
				# file = "files/txt/"  + "example.txt"
				file  =  join(self.dirBaseOriginal, filename)
				# Name of the file without the original extension
				filenoext =  splitext(filename)[0]
				# Destionation file where the file  will be compressed
				# example.txt  ===> example.zip
				filec = dirCompressed +  filenoext + '.'  + formats[method]
				# Create the compression  object and executes  it
				compression = Compression(method, file, filec)
				# Adds the compression time to the list of times for the 
				# compression format
				timesCompression[method].append(compression.time)

		return timesCompression

	def __decompression__(self):
		timesDecompression = {}
		for method in self.methods:
			timesDecompression[method] = []
			dirCompressed = self.dirBaseCompression + method + "/"
			dirdecompressed = self.dirBaseDecompression + method
			for filename in self.files:
				filext =  splitext(filename)[0] + '.' + formats[method]
				file  =  join(dirCompressed, filext)
				decompression = Decompression(method, file, dirdecompressed)
				timesDecompression[method].append(decompression.time)

		return timesDecompression

	def __method_rate__(self, method):
		self.rate[method] = []
		dirCompressed = self.dirBaseCompression + method + "/"
		i = 0
		for filename in self.files:
			filenoext = splitext(filename)[0]
			filec = dirCompressed +  filenoext + '.' + formats[method]
			# compare both files
			comp = os.stat(filec).st_size/1024
			unco = self.fileSizes[i]
			i += 1
			rate = 100 * comp/unco
			self.rate[method].append(rate)

	def  __calc_sizes__(self):
		for file in self.files:
			froute = join(self.dirBaseOriginal, file)
			self.fileSizes = np.append(self.fileSizes, os.stat(froute).st_size/1024)

		
		# sorts files by their size
		inds = self.fileSizes.argsort()
		self.fileSizes = self.fileSizes[inds]
		self.files = self.files[inds]

	# Limpia los archivos antes comprimidos y descomprimidos
	# para evitar colisiones al comprimir y descomprimir nuevamente
	def __reset__(self, compression = False, decompression = False):
		for method in self.methods:
			if compression:
				pathCom = self.dirBaseCompression + method
				if os.path.isdir(pathCom):
					sh.rmtree(pathCom)

			if decompression:
				pathDe = self.dirBaseDecompression + method
				if os.path.isdir(pathDe):
					sh.rmtree(pathDe)

class Compression:

	def __init__(self, method, file, destination):
		self.method = method
		self.file = file
		self.destination = destination
		# Asyncronously runs the compression method
		# the method originally runs in parallel, but
		# as  it's run and defined as asyncronous, the program
		# will wait for it to finish.
		start = time.time()
		asyncio.run(self.__compress__())
		end = time.time()
		self.time = end - start

	async def __compress__(self):
		subprocess.call(['7z', "a", '-t' + self.method, self.destination , self.file])

class Decompression:

	def __init__(self, method, file, destination):
		self.method = method
		self.file = file
		self.destination = destination
		# Asyncronously runs the decompression method
		# the method originally runs in parallel, but
		# as  it's run and defined as asyncronous, the program
		# will wait for it to finish
		start = time.time()
		asyncio.run(self.__decompress__())
		end = time.time()
		self.time = end - start

	# Defines the asyncronous process for decompressing files
	# it executes  the command line necessary for this job
	async def __decompress__(self):
		subprocess.call(['7z', "e", '-t' + self.method,  self.file, '-o' + self.destination])
